package com.ideas2it.empresa.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.empresa.dao.DesignationDAO;
import com.ideas2it.empresa.dao.impl.DesignationDAOImpl;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.service.DesignationService;

/**
 * The DesignationServiceImpl is the implementationof the DesignationService 
 * interface it implements the read method to read the designations from the 
 * designation record.
 */
public class DesignationServiceImpl implements DesignationService {
    DesignationDAO designationDaoImpl = new DesignationDAOImpl();

    /**
     * (non-javadoc)
     * 
     * @see com.ideas2it.empresa.service.Designationservice#getAllDesignations()
     */
    @Override
    public List retrieveAllDesignations() throws AppException, DBException {
        List<Designation> designations = designationDaoImpl.retireveAllDesignations();
        if (null != designations) {
            throw new AppException("No designation found");
        }
        return designations;
    }

    /**
     * (non-javadoc)
     * 
     * @see 
     * com.ideas2it.empresa.service.Designationservice#retrieveDesignationById(
     * int)
     */
    @Override
    public Designation retrieveDesignationById(int id) throws AppException, 
        DBException 
    {
        Designation designation = designationDaoImpl.retrieveDesignationById(id); 
        if (null != designation) {
            throw new AppException("No designation found");
        }
        return designation;
    }
}
