package com.ideas2it.empresa.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.time.format.DateTimeParseException;
import java.sql.SQLException;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.dao.DesignationDAO;
import com.ideas2it.empresa.dao.impl.DesignationDAOImpl;
import com.ideas2it.empresa.dao.EmployeeDAO;
import com.ideas2it.empresa.dao.impl.EmployeeDAOImpl;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.service.EmployeeService;

/**
 * <p>EmployeeServiceImpl is the implementation of the EmployeeService interface
 * it implements all methods in the interface to process all the business logic  
 * of the employee, gets the input for operation validates as the first step  
 * then the actual operation is carried invalid data should not processed and to 
 * ensure the reliability of the data in the data store.
 *
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDAO employeeDAOImpl = new EmployeeDAOImpl();

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#addEmployee(String,
     *      String, String, String, String)
     */
    @Override
    public Map<String, Object> addEmployee(Employee employee) throws 
        AppException, DBException
    {
        StringBuilder message = new StringBuilder();
        Map<String, Object> status = new HashMap<String, Object>();
        boolean isValid = employeeValidater(employee.getName(), 
                              employee.getEmail(), employee.getPhone(), 
                              employee.getDOB(), message);
        if(isValid) {
            boolean emailExists = isEmailExists(employee.getEmail());
            boolean phoneExists = isPhoneExists(employee.getPhone());
            if(!emailExists && !phoneExists) {
                int id = employeeDAOImpl.insert(employee); 
                message.append("\nInserted successfully and employee" 
                                        + "id is " + id);
                status.put(Constant.ID, id);
                status.put(Constant.ERROR_STATE, Constant.SUCCESS);
            } else {
                if(emailExists) {
                    message.append("\nEmail already exists.");
                }
                if(phoneExists) {
                    message.append("\nPhone number already exists.");
                }
                throw new AppException(message.toString());
            }   
        } else {
            throw new AppException(message.toString());
        }
        return status;
    }
 
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#updateNameById(
     *          String, String) 
     */	
    @Override
    public String updateName(Employee employee) throws AppException, 
        DBException  
    {
        if(!CommonUtil.isLiteral(employee.getName())) {
            throw new AppException("Invalid name.");    
        } else {
            employeeDAOImpl.updateNameById(employee);
        }
        return "Successfully updated"; 
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#updatePhoneById(
     *      String, String)
     */
    @Override
    public String updatePhone(Employee employee) throws AppException, 
        DBException  
    {
        if(!CommonUtil.isPhone(employee.getPhone())) {
            throw new AppException("Invalid email.");    
        } else if (null != employeeDAOImpl.getEmployeeByPhone(
                      employee.getPhone())) 
        {
            throw new AppException("phone already exists");
        } else {
            employeeDAOImpl.updatePhoneById(employee);
        }
        return "Successfully updated";    
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#updateEmailById(
     *      String, String)
     */
    @Override
    public String updateEmail(Employee employee) throws AppException, 
        DBException 
    {
        if(!CommonUtil.isEmail(employee.getEmail())) {
            throw new AppException("Invalid email.");    
        } else if (null != employeeDAOImpl.getEmployeeByEmail(
                      employee.getEmail())) 
        {
            throw new AppException("Email already exists");
        } else {
            employeeDAOImpl.updateEmailById(employee);
        }
        return "Successfully updated";        
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#getAllEmployees()
     */
    @Override
    public List<Employee> getAllEmployees() throws AppException {
        List<Employee> employees = employeeDAOImpl.retrieveEmployees();
        return employees;
    } 

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#deleteEmployeeById(
     *      String)
     */
    @Override
    public String deleteEmployeeById(Employee employee) throws DBException {
        employeeDAOImpl.delete(employee);
        return "Deleted successfully";
    }

    /**
     * (non-javadoc)
     * 
     * @see com.ideas2it.empresa.service.EmployeeService#getEmployeeById(
     *      String)
     */
    @Override
    public Map<String, String> getEmployeeById(String id) throws
        AppException
    { 
        Map<String, String> status = new HashMap<>();
        try {
            if(CommonUtil.isNumeric(id)){
                if(employeeDAOImpl.isEmployeeExists(Integer.parseInt(id))) {
                    status.put(Constant.ERROR_STATE, Constant.SUCCESS);
                    status.put(Constant.MESSAGE, "Employee found.");
                } else {
                    status.put(Constant.ERROR_STATE, Constant.FAILURE);
                    status.put(Constant.MESSAGE, "No employee found.");
                }
            } else {
                status.put(Constant.ERROR_STATE, Constant.FAILURE);
                status.put(Constant.MESSAGE, Constant.INVALID_NO);
            }
        } catch (NumberFormatException exception) {
            throw new AppException("Invalid number Exception" , exception);
        }
        return status;
    } 	

    /**
     *                                              TODO should be changed
     */
    public Employee getEmployeeById(int id) throws AppException, DBException {
        Employee employee = employeeDAOImpl.getEmployeeById(id);
        if(null == employee) {
            throw new AppException("No Employee Found ");
        }
        return employee;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.
     *          EmployeeService#assignProjectToEmployee(int, int) 
     */
    @Override
    public String assignProjectToEmployee(int employeeId, int projectId) throws 
        AppException 
    {
        String status;
        int result = employeeDAOImpl.assignProjectToEmployee(employeeId, 
                         projectId);
        if(result > Constant.ZERO) {
            status = "Assigned successfully";
        } else {
            status = "Assigning failed.";
        }
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#getEmployeesByProject(
     *          int)                   
     */
    @Override
    public List<Employee> getEmployeesByProject(int projectId) throws 
        AppException 
    {
        return employeeDAOImpl.retrieveEmployeesByProject(projectId);
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService#getAllDesignations()
     */
    @Override
    public List<Designation> getAllDesignations() throws AppException,
        DBException 
    {
        DesignationDAO designationDAOImpl = new DesignationDAOImpl();
        return designationDAOImpl.retireveAllDesignations(); 
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.EmployeeService
     *          #validateEmployeeCredentials(Employee)
     */
    @Override
    public boolean validateEmployeeCredentials(Employee employee) throws 
        AppException, DBException 
    {
        Employee emp = getEmployeeById(employee.getId());
        if (emp.getPassword().equals(employee.getPassword())) {
            return true;
        }   
        return false;
    }

    /**
     * <p>Validates employee details and returns true if all details are valid, 
     * false otherwise.
     * @param name - name to be validated 
     *                  eg.steven3 is not valid as it contains number.
     * @param email - email to be validated 
     * @param phone - phone to be validated 
     * @param DOB - DOB to be validated
     * @param errorMessage - errorMessage to append errors when invalid data 
     *                          is checked.
     * @return - true if all details are valid and false otherwise. 
     * @throws - AppException if date is not valid.
     */ 
    private boolean employeeValidater(String name, String email, String phone, 
        String DOB, StringBuilder message) throws AppException 
    {
        boolean successState = Constant.TRUE;

        if(!CommonUtil.isLiteral(name)) {
            message.append("\nName should contain only character");
            successState = Constant.FALSE;
        }
        if(!CommonUtil.isPhone(phone)) {
            message.append("\nPhone should be 10 digits and only numbers");
            successState = Constant.FALSE;
        }
        if(!CommonUtil.isEmail(email)) {
            message.append("\nEmail invalid example(example@domain.com)");
            successState = Constant.FALSE;
        }
        try {
            if(!CommonUtil.isDate(DOB)) {
            message.append("\nDOB should be in format YYYY-MM-DD");
            successState = Constant.FALSE;
            } else if (CommonUtil.calculateAge(DOB) < Constant.TWENTY) {
                message.append("Minimum age should be 20 to work.");
                successState = Constant.FALSE;
            }
        } catch (DateTimeParseException exception) {
            throw new AppException("Exception while parsing date " + DOB,
                                            exception);
        }

        return successState;
    }

    /**
     * <p>Takes email address and checks whether email is already exists for
     * another employee if exists returns true, false otherwise.
     * @param email email address to be searched.
     * @return true if phone exists, false otherwise.
     * @throws AppException if email cannot be readed.
     */
    private boolean isEmailExists(String email) throws DBException {
        return (null != employeeDAOImpl.getEmployeeByEmail(email)) ? true 
                   : false; 
    }      

    /**
     * <p>Takes phone number and checks whether phone is already exists for
     * another employee if exists returns true, false otherwise.
     * @param phone phone number to be searched.
     * @return true if phone exists, false otherwise.
     * @throws AppException if phone cannot be readed.
     */
    private boolean isPhoneExists(String phone) throws DBException {
        return (null != employeeDAOImpl.getEmployeeByPhone(phone)) ? true 
                   : false; 
    } 
}

