package com.ideas2it.empresa.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;

/**
 * The ProjectService interface exposes the Create, read, delete operations of 
 * the project.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface ProjectService {

    /**
     * Takes project details and adds a project to the project record. 
     * @param name name of the project to be inserted.
     * @param description description of the project to be inserted
     * @param clientId id of the client which is unique and used to identify 
     *                      the client.
     * @return map with id of the inserted project and status message whether 
     *              inserted or not. 
     * @throws AppException if project cannot be added.                  
     */ 
    Map<String, Object> addProject(String name, String description, 
        int clientId) throws AppException;

    /**
     * Gets all projects from the projects table and returns it as a list.
     * @return projects list of projects
     * @throws AppException if cannot retrive all projects.                 
     */
    List<Project> getAllProjects() throws AppException;

    /**
     * Gets a particular project from the projects from a particular client 
     * identified
     * by the client's id table and returns it as a list.
     * @param clientId id if the client which is unique to every client.
     * @return projects  list of projects
     * @throws AppException if cannot retrive all projects.                 
     */
    List<Project> getAllProjectsByClientId(int clientId) throws AppException;

    /**
     * Gets a particular project from the projects record identified by the 
     * project's id and returns the project.
     * @param projectId id if the client which is unique to every client.
     * @param clientId id if the client which is unique to every client.
     * @return status status message success or failure and project as map. 
     *      project is found with the project and client id.
     * @throws              AppException if cannot retrive all projects.                 
     */
    Map<String, Object> getProjectById(int projectId, int clientId) throws  
        AppException;

    /**
     * Gets a particular project from the projects record identified by the 
     * project's name and returns the project.
     * @param name project's name being searched.
     * @param clientId id if the client which is unique to every client.
     * @return status status message success or failure and project as map. 
     * @throws AppException if cannot retrive all projects.                 
     */
    Map<String, Object> getProjectByName(String name, int clientId) throws 
        AppException;

    /**
     * Takes project id and deletes a project from the project record.
     * @param id  id of the project which is unique to every project. 
     * @return  zero if no project is deleted, no of projects deleted otherwise.
     * @throws AppException if project cannot be deleted.                 
     */
    String deleteProject(int id) throws AppException;

    /** 
     * Validates the id if not valid returns failure message as map otherwise 
     * checks if project is existing with the id and returns success or failure 
     * map and corresponding message.  
     * @param id id of the project which is unique to every project. 
     * @return status success message if project is existing or error message,
                        if not existing.
     * @throws AppException if cannot retrive a project.                 
     */
    Map<String, String> hasProjectForId(String id) throws AppException;

    /**
     * Checks whether the project is existing for the given id and returns true 
     * if project is existing in the record false otherwise.
     * @param projectId id of the project which is unique to every project. 
     * @return true if project is existing false otherwise
     * @throws AppException if project cannot be checked whether exists.
     */
    boolean isProjectExists(int projectId) throws AppException; 

    /**
     * Takes client id and returns the no of projects the client owns.
     * @param clientId id of the client which is unique and used to identify 
     *        the client.   
     * @return noOfProjects  returns the no of projects the particular client 
     *         owns.
     * @throws AppException if no of projects cannot be retrieved for the 
     *         client 
     */ 
    int getNoOfProjects(int clientId) throws AppException;
    
    /**
     * Gets project from the project record with the project id.
     * @param id id of the employee to be retrieved
     * @return project project for the given id.
     * @throws DBException if project cannot be retrieved.
     * @throws AppException if no project is found.
     */
    public Project getProjectById(int id) throws AppException, DBException;

    /**
     * <p>Assigns employee to the project.
     * @param project prooject in which employee ro be assigned
     * @param employee employee to be assigned in the project.
     * @return message when employee is assigned to project successfully.
     * @throws AppException if employee cannot be assigned to project.
     */
    public String assignProjectToEmployee(Project project, Employee employee) 
        throws DBException;
} 
