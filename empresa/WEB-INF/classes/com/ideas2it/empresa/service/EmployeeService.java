package com.ideas2it.empresa.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;


/**
 * The EmployeeService interface exposes all the CRUD and business operation of 
 * the employee.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface EmployeeService {

    /**
     * Adds employee to the employee record and returns success message if added
     * and error message if not added successfully.   
     * @employee employee to be added to the employee record
     * @return status - succes message if successfully added along with 
     *     added employee id else error message with errors occurred. 
     * @throws AppException if employee's data are invalid.  
     * @throws AppException if employee cannot be inserted.                        
     */
    Map<String, Object> addEmployee(Employee employee) throws AppException, 
       DBException;

    /**
     * Gets all designations from the designation record and returns the list of
     * designations.
     * @return List of all available designations.
     * @throws AppException if designations cannot be retrieved.
     */
    List<Designation> getAllDesignations() throws AppException, DBException;

    /**
     * Takes employee id and name validates name, if valid updates employee's 
     * name and returns success message if updated successfully else error 
     * message.   
     * @param id -id of the employee which is unique to every employee.
     * @param name - new name of the employee	 
     * @return status - Success message if name is updated successfully, 
     *                      failure message other wise.
     * @throws AppException if name cannot be updated.
     */
    String updateName(Employee employee) throws AppException, DBException;

    /**
     * Updates employee's phone and returns success message if phone is updated  
     * successfully otherwise error message.   
     * @param id -id of the employee which is unique to every employee.
     * @param phone - phone number of the employee which is unique and can be 
     *                    used to communicate the employee. 
     * @return status - Success message if phone is updated successfully, 
     *                      failure message other wise.
     * @throws AppException if phone number cannot be updated.
     */
    String updatePhone(Employee employee) throws AppException, DBException;

    /**
     * Updates employee's email and returns success message if email is updated
     * successfully otherwise error message.   
     * @param id -id of the employee which is unique to every employee.
     * @param email - email address of the employee which is unique and can be 
     *                    used to communicate the employee. 
     * @return status - Success message if email is updated successfully, 
     *                      failure message other wise.
     * @throws AppException if email cannot be updated.
     */
    String updateEmail(Employee employee) throws AppException, DBException;

    /**
     * gets all employees from record and returns list of the employees.
     * @return - List of all employee objects 
     * @throws AppException if employee cannot be retrieved.
     */
    List<Employee> getAllEmployees() throws AppException;

    /**
     * Deletes employee from the employee record and returns success message if 
     * deleted successfully otherwise error message.      
     * @param employee employee to be deleted from the record. 
     * @throws AppException if employee cannot be deleted.                 
     */
    String deleteEmployeeById(Employee employee) throws DBException;

    /** 
     * Validates the id if not valid returns failure message as map otherwise 
     * checks if employee is existing with the id and returns map success or 
     * failure and corresponding message.  
     * @param id -id of the employee which is unique to every employee.
     * @return status - success message if employee is existing or error 
                           message, if not.    
     * @throws AppException if employee cannot be readed.             
     */
    Map<String, String> getEmployeeById(String id) throws    
        AppException;

    /**
     * Gets the employee for the given id and returns the employee.
     * @param id id of the the which is unique for every employee.  
     * @return employee employee for the given id
     * @throws DBException if employee cannot be retrieved for the given id.
     */
    Employee getEmployeeById(int id) throws AppException, DBException;

    /**
     * Takes employee id and project id and assigns a project to the employee. 
     * @param employeeId id of the employee which is unique to every employee.
     * @param projectId id of the project which is unique to every project.
     * @return status success message if assigned successfully, error message 
                     if not assigned successfully, 
     * @throws AppException if employee cannot be assigned to a project.
     */
    String assignProjectToEmployee(int employeeId, int projectId) throws 
        AppException;

    /**
     * Gets all employees from the record belonging to the project identified by 
     * the project id.
     * @param projectId  of the project from which all employees to be read.
     * @return employees list of the employees in the project.
     * @throws AppException if employees cannot retrieved by project.
     */
    List<Employee> getEmployeesByProject(int projectId) throws 
        AppException;

    /**
     * Takes employee id and password and validates his / her credentials.
     * @param employee user-id and password of the employee to validated
     * @return true if employees credentials are exist false if not.
     */
    boolean validateEmployeeCredentials(Employee employee) throws AppException, 
        DBException;
}
