package com.ideas2it.empresa.controller; 

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.common.EmpresaLogger;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;
import com.ideas2it.empresa.service.AddressService;
import com.ideas2it.empresa.service.impl.AddressServiceImpl;

/**                                                                                
 * The AddressController gets users inputs controls the logic and handles the 
 * user request then responds the user with the result for the operation for 
 * the requested operation. 
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class AddressController {
    private AddressService addressServiceImpl;

    /**
     * Adds a person's address to the address record.
     * @param address postal address of the owner which can be use to
     *              communicate him / her.
     * @return message - success message if added success fully else failure  
     *                      message
     */
    public String addAddress(Address address) {
        String message = new String();
        AddressService addressServiceImpl = new AddressServiceImpl();
        try {
            message = addressServiceImpl.addAddress(address);
        } catch(AppException appException) {
            message = "Something went wrong couldn't add address.";
            EmpresaLogger.log(appException);
        }
        return message;        
    }

    /**
     * Takes owner's address and checks whether address for the owner with the 
     * same type already exists.
     * @param address containing address details.
     * @return status - whether type is address of same type available and error
     *                      message 
     */
    public Map<String, String> getTypeStatus(Address address) {
        Map<String, String> status = new HashMap<>();
        AddressService addressServiceImpl = new AddressServiceImpl();
        try {
            status = addressServiceImpl.getTypeStatus(address);
        } catch (AppException appException) {
            EmpresaLogger.log(appException);
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
            status.put(Constant.MESSAGE, "Something went wrong couldn't check"  
                         + "address.");
        }
        return status;    
    }


    /**
     * Takes owner's address and updates the address of the same type either 
     * primary or secondary
     * @param address postal address of the owner which can be use to
     *              communicate him / her.
     * @return message whether updates successfully or not
     */
    public String updateAddress(Address address) {
        String message = new String();
        AddressService addressServiceImpl = new AddressServiceImpl();
        try {
            message = addressServiceImpl.updateAddress(address);
        } catch(AppException appException) {
            message = "Something went wrong couldn't add address.";
            EmpresaLogger.log(appException);
        }
        return message;
    }
}
