package com.ideas2it.empresa.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.common.EmpresaLogger;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.service.EmployeeService;
import com.ideas2it.empresa.service.impl.EmployeeServiceImpl;
import com.ideas2it.empresa.service.ProjectService;

/**                                                                                
 * The EmployeeController gets users inputs controls the logic and handles the 
 * user request then responds the user with the result for the operation for the
 * requested operation. 
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class EmployeeController {
    private EmployeeService employeeServiceImpl = new EmployeeServiceImpl();

    /**
     * Adds employee to the employee record and returns the result. Returns 
     * success message if added successfully otherwise error message.    
     * @param name - name of the employee. 
     * @param email - email address of the employee which can be used to 
     *                  communicate him / her. 
     * @param phone - phone number of the employee which can be used to 
     *                  communicate him / her. 
     * @param DOB - DOB of the employee to be added. 
     * @param designationId - designation id which is used to identify the 
     *                           designation.
     * @return status   success message if successfully added else error 
     *                  message with errors occurred.     
     */
    public Map<String, Object> addEmployee(Employee employee) throws 
        AppException  
    {
        Map<String, Object> status = new HashMap<>();
        try {
            status = employeeServiceImpl.addEmployee(employee);
        } catch (AppException e) {
            EmpresaLogger.log(e);   
            throw new AppException(e.getMessage());
        } catch (DBException e) {
            EmpresaLogger.log(e);                                  
            throw new AppException("Something went wrong couldn't insert.");
        }
        return status;
    }
                      
    /**
     * <p>Gets all designations from the designation record and returns a list  
     * of designations.
     * @return List of all designations available.
     * @throws AppException if designations cannot be retrieved.
     */
    public List<Designation> getAllDesignations() throws AppException {
        List<Designation> designations;
        try {
            designations = employeeServiceImpl.getAllDesignations();
        } catch (AppException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong couldn't fetch "
                                      + "details.");
        } catch (DBException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong, not updated");
        }
        return designations;
    }         
                                                                  
	/**                                                                            
     * <p>Updates employee name to the employee record and returns success  
     * message if updated successfully otherwise error message.   
     * @param id - id of the employee which is unique to every employee.			
     * @param name - new name to be updated.
     * @return message - Success message if updated successfully, failure 
     *                      message other wise.
     */
    public String updateNameById(Employee employee) throws AppException{
        String message = new String();
        try {
            message = employeeServiceImpl.updateName(employee);
        } catch (DBException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong, not updated");
        }
        return message;
    }

    /**
     * <p>Updates employee email to the employee record and returns success 
     * message if updated successfully otherwise error message.   
     * @param id - id of the employee which is unique to every employee.			
     * @param email - new email to be updated.
     * @return status - Success message if updated successfully, failure 
     *                      message other wise.
     */
    public String updateEmailById(Employee employee) throws AppException {
        String message = new String();
        try {
            message = employeeServiceImpl.updateEmail(employee);
        } catch (DBException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong, not updated");
        }
        return message;
    }

	/**
     * <p>Updates employee phone to the employee record and returns success 
     * message if updated successfully otherwise error message.   
     * @param id - id of the employee which is unique to every employee.			
     * @param phone - new phone to updated.
     * @return status - Success message if updated successfully, failure 
     *                      message other wise.
     */
    public String updatePhoneById(Employee employee) throws AppException {
        String message = new String();
        try {
            message = employeeServiceImpl.updatePhone(employee);
        } catch (DBException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong, not updated");
        }
        return message;
    }                                                                             

    /**
     * <p>Deletes employee from the employee record and returns success message if 
     * deleted successfully otherwise error message.   
     * @param employee employee to be deleted form the employee record.
     * @return status - Success message if deleted successfully, failure 
     *                      message other wise.
     */
    public String deleteEmployeeById(Employee employee) throws AppException {
        String message = new String();
        try {
            message = employeeServiceImpl.deleteEmployeeById(employee);
        } catch (DBException e) {
            EmpresaLogger.log(e);   
            throw new AppException("Something went wrong, employee for id : "
                                      + employee.getId() + " not deleted.");         
        }
        return message;
    }

    /**
     * <p>Gets all employees from record and returns the list of all employees.
     * @throws AppException if employees cannot be retrieved.
     * @return List of all employee objects 
     */
    public List<Employee> getAllEmployees() throws AppException {
        List<Employee> employees;
        try {
            employees = employeeServiceImpl.getAllEmployees();
        } catch (AppException appException) {
            EmpresaLogger.log(appException);
            throw new AppException("Something went wrong couldn't fetch "
                                      + "details.");
        }
        return employees;
    }    

    /**
     * <p>Gets all employees in a particular project and returns the list
     * of all employees.
     * @param projectId  id of the project which is unique to every project.			
     * @return List of all employee objects who are associated with the project. 
     * @throws AppException if employees cannot be retrieved in the project.
     */
    public List<Employee> getEmployeesByProject(int projectId) throws 
        AppException 
    {
        List<Employee> employees = new ArrayList<>();
        try {
            employees = employeeServiceImpl.getEmployeesByProject(projectId);
        } catch (AppException appException) {
            EmpresaLogger.log(appException); 
            throw new AppException("Something went wrong sorry we couldn't get " 
                                      + "employees for the project:" 
                                      + projectId);   
        }
        return employees;
    } 

    /**
     * <p>Gets an employee with the given id. Returns map with employee name and 
     * project id else with error message.
     * @param id - id of the employee which is unique to every employee.			
     * @return status - map with success or failure state and corresponding 
     *                  success or failure message.
     */
    public Map<String, String> getEmployeeById(String id) {
        Map<String, String> status = new HashMap<>();
        try {
            status = employeeServiceImpl.getEmployeeById(id);
        } catch (AppException appException) {
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
            status.put(Constant.MESSAGE, "Something went wrong couldn't get "  
                           + "employee for id = " + id);
            EmpresaLogger.log(appException);
        }
        return status;
    }
    
    /**
     * Gets Employee for the given employee id and returns it.
     * @param id id of the employee which is unique for every employee
     * @return employee employee for the given id 
     * @throws AppException if employee cannot be retrieved or not found for the
     *         given id.
     */
    public Employee getEmployeeById(int id) throws AppException {
        Employee employee = null;
        try {
            employee = employeeServiceImpl.getEmployeeById(id);
        } catch (DBException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't get employee."
                                    );              
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("No employee found for the given id.");        
        }
        return employee;
    }    

    /**
     * <p>Adds employee to the existing project.
     * @param employeeId to be added in the project.
     * @param projectId in employee to be added
     * @return message whether project is assigned successfully or not.
     */
    public String assignProjectToEmployee(int employeeId, int projectId) {
        String message = new String();
        try {
            message = employeeServiceImpl.assignProjectToEmployee(employeeId, 
                                                                  projectId);
        } catch(AppException appException) {
            message = "Something went wrong couldn't assign employee to " + 
                          "project"; 
            EmpresaLogger.log(appException);
        }
        return message;
    }
}
