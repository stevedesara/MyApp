package com.ideas2it.empresa.exception;

/**
 * The AppException handles all the application related exceptions and gets 
 * other exceptions thrown, wraps it and customizes it as Application Exception.
 * 
 * @author Rajasekar 
 * created on 2017/07/11
 */
public class AppException extends Exception {
    
    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(String message) {
        super(message);
    }

    public AppException() { }

    public AppException(Throwable cause) {
        super(cause);
    }
    
    public AppException(String message, Throwable cause, 
        boolean enableSuppression, boolean writableStackTrace) 
    {
        super(cause);
    }
}
