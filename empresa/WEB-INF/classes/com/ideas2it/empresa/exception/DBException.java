package com.ideas2it.empresa.exception;

/**
 * The AppException handles all the DB related exceptions and customizes it as 
 * Application Exception.
 * 
 * @author Rajasekar 
 * created on 2017/07/26
 */
public class DBException extends Exception {
    
    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(String message) {
        super(message);
    }

    public DBException() { }

    public DBException(Throwable cause) {
        super(cause);
    }
    
    public DBException(String message, Throwable cause, 
        boolean enableSuppression, boolean writableStackTrace) 
    {
        super(cause);
    }
}
