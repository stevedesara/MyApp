package com.ideas2it.empresa.dao;

import java.util.List;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Project;

/**
 * The ProjectDAO exposes all the CRUD operations related to the project.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface ProjectDAO {

    /**
     * Inserts project data into the project record and returns project id if 
     * inserted successfully else zero.
     * @param project Project Object containing project details.
     * @return projectId id of the project inserted.
     * @throws AppException if the project cannot be inserted.
     */
    int insert(Project project) throws AppException;

    /**
     * Retrieves all projects from the record and sets it to project object and 
     * returns a list of project object.
     * @return projects projects list containing project object.
     * @throws AppException if projects cannot be retrieved.
     */
	List<Project> retriveAllProjects() throws AppException;

    /**
     * Deletes project from the record identified with the given id.Returns zero 
     * if none of the record is deleted otherwise number of records affected.
     * @param id id of the proejct to be deleted. 
     * @return integer zero if no record is deleted, else no of rows deleted.
     * @throws AppException if project is cannot deleted.
     */
    int delete(Project project) throws AppException;

    /**
     * Checks whether the project is existing for the given id and returns true 
     * if project is existing in the record else false.
     * @param projectId project id which is unique for each project.
     * @return boolean true if project is existing false otherwise
     * @throws AppException if project cannot be checked whether exists.
     * @return boolean true if project exists else false.
     */
    boolean isProjectExists(Project project) throws AppException;

    /**
     * Retrieves all projects from the projects record of a particular client 
     * identified by the client's id table and returns it as a list.
     * @param clientId client's id whose projects are to be retrieved.
     * @return projects list of projects
     * @throws AppException if cannot retrive all projects.                 
     */
    List<Project> retrieveAllProjects(int clientId) throws AppException;

    /**
     * Retrievs a particular project from the projects record identified by the 
     * project's id and returns the project.
     * @param projectId project's id which is unique to every project.
     * @param clientId client's id which is unique to every client.
     * @return Project - returns project object.
     * @throws AppException if cannot retrive all projects.                 
     */
    Project retrieveProjectById(int projectId, int clientId) throws AppException;

    /**
     * Retrieves a particular project from the projects record identified by the 
     * project's name and returns the project.
     * @param name name of the project to be searched
     * @param clientId client's id which is unique to every client.
     * @return Project - returns project object.
     * @throws AppException if cannot retrive all projects.                 
     */
    Project retrieveProjectByName(String name, int clientId) throws AppException;

    /**
     * Takes client id and returns the no of projects that client owns from the  
     * client record.
     * @param clientId id of the client which is unique and used to identify 
     *                      the client.   
     * @return noOfProjects returns the no of projects the particular client 
     *                            owns.
     * @throws AppException if no of projects cannot be retrieved for the 
     *                            client 
     */ 
    int getNoOfProjects(int clientId) throws AppException;

    /**
     * Assigns a employee or list of employees to the project.
     * @param employeeId employee id to update project id.
     * @param projectId project id to be added in employee tabel.
     * @throws AppException if employee cannot be assigned to project.
     * @return zero if the employee is not assigned to the project else one.                 
     */
    void assignProjectToEmployee(Project project) throws DBException;

    /**
     * Gets project from the project record with the project id.
     * @param id id of the employee to be retrieved
     * @return project project for the given id.
     * @throws DBException if project cannot be retrieved.
     */
    public Project getProjectById(int id) throws DBException;
}
