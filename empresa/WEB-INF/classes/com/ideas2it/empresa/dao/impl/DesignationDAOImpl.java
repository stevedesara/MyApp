package com.ideas2it.empresa.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.HibernateException;

import com.ideas2it.empresa.common.SessionGenerator;
import com.ideas2it.empresa.dao.DesignationDAO;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;

/**
 * The DesignationDAOImpl is the implementation of the DesignationService 
 * interface it implements the read method to retrieve all desingations from the
 * designation record.
 */
public class DesignationDAOImpl implements DesignationDAO {
    private static SessionFactory factory = SessionGenerator.getFactory(); 

    /**
     * (non-javadoc)
     * @see com.ideas2it.empresa.dao.DesignationDAO#retrieveAllDesignations()
     */
    @Override
    public List<Designation> retireveAllDesignations() throws DBException {            
        List<Designation> designations = null;
        try (Session session = factory.openSession()){
            Criteria criteria = session.createCriteria(Designation.class);
            designations = criteria.list();
        } catch (HibernateException e) {
            throw new DBException("Exception while retrieving all designation");
        }     
        return designations; 
    }

    /**
     * (non-javadoc)
     * @see com.ideas2it.empresa.dao.DesignationDAO#retrieveDesignationById(int)
     */
    @Override
    public Designation retrieveDesignationById(int id) throws DBException {
        Designation designation = null;
        try (Session session = factory.openSession()){
            designation = session.get(Designation.class, new Integer(id));
        } catch (HibernateException e) {
            throw new DBException("Exception while retrieving designation by id" 
                                    + id);
        }     
        return designation;       
    }
}
