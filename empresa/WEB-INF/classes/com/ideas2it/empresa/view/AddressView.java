package com.ideas2it.empresa.view;

import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.controller.AddressController;
import com.ideas2it.empresa.model.Address;

/**
 * The AddressView acts as an interface to the user and provides the user with 
 * options of the operations. It gets input from the user for the operations 
 * then responds to the user. 
 * 
 * @author Rajasekar
 * created on 19/07/2017
 */
public class AddressView { 
    private static Scanner scanner = new Scanner(System.in);
    private AddressController addressController;

    /**
     * Gets an address of a person and adds address to address record.
     * @param ownerId - id of the owner which is unique to every owner 
     * @param owner - owner who owns the address may be client, employee or 
     *                    employer
     */
    public void addAddress(int ownerId, String owner) {
        addressController = new AddressController();
        System.out.println("Enter address type:\n1.Primary\n2.Secondary");
        String type = scanner.next();
        if (CommonUtil.isLiteral(type)) {
            if (type.equalsIgnoreCase(Constant.PRIMARY) || 
                   type.equalsIgnoreCase(Constant.SECONDARY)) 
            {
                Map<String, String> status = addressController.getTypeStatus(
                                                 new Address(ownerId, owner, 
                                                 type.toLowerCase()));
                if(status.get(Constant.ERROR_STATE).equals(Constant.FAILURE)) {

                    System.out.println(addressController.addAddress(getAddress(
                                           ownerId, owner, type)));                   
                } else {
                    System.out.println(status.get(Constant.MESSAGE));            
                }
            } else {
                System.out.println(Constant.INVALID_CHOICE);
            } 
        } else {
            System.out.println(Constant.INVALID_NO);
        }       
    }

    /**
     * Gets an address of a person and updates the address if exists already 
     * and displays succes or failure message.
     * @param ownerId - id of the owner which is unique to every owner 
     * @param owner - owner who owns the address may be client, employee or 
     *                    employer
     */
    public void updateAddress(int ownerId, String owner) { 
        addressController = new AddressController();
        System.out.println("Enter address type you want to update :\n1.Primary"
                               + "\n2.Secondary");
        String type = scanner.next();
        if (CommonUtil.isLiteral(type)) {
            if (type.equalsIgnoreCase(Constant.PRIMARY) || 
                   type.equalsIgnoreCase(Constant.SECONDARY)) 
            {
                Map<String, String> status = addressController.getTypeStatus(
                                                 new Address(ownerId, owner, 
                                                 type.toLowerCase()));
                if(status.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) {

                    System.out.println(addressController.updateAddress(
                                           getAddress(ownerId, owner, type)));                   
                } else {
                    System.out.println(status.get(Constant.MESSAGE));            
                }
            } else {
                System.out.println(Constant.INVALID_CHOICE);
            } 
        } else {
            System.out.println(Constant.INVALID_NO);
        }
    }

    /**
     * Takes owner's id and owner type,gets address details from the user and 
     * returns it as address object.
     * @param ownerId  id of the owner which is unique to every owner 
     * @param owner owner who owns the address may be client, employee or 
     *                    employer
     * @param type type of the address either primary or secondary
     * @return Address  address, postal address of the owner which can be used to 
     *          communicate him / her. 
     */
    public Address getAddress(int ownerId, String owner, String type) {
        System.out.println("Enter address line 1");
        String doorNo = scanner.next();
        System.out.println("Enter street");
        String street = scanner.next();
        System.out.println("Enter city");
        String city = scanner.next();
        System.out.println("Enter zipcode");
        String zipcode = scanner.next();
        return new Address(doorNo, street, city, zipcode, ownerId, owner, 
                              type.toLowerCase());    
    }
}
