package com.ideas2it.empresa.view;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.controller.ClientController;
import com.ideas2it.empresa.controller.EmployeeController;           
import com.ideas2it.empresa.controller.ProjectController;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;             
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.view.ProjectView;

/**
 * The ClientView acts as an interface to the user and provides the user with 
 * options of the operations. It gets input from the user for the operations 
 * then responds to the user with the results.
 *  
 * @author Rajasekar
 * created on 14/07/2017
 */
public class ClientView {
    private static Scanner scanner = new Scanner(System.in);
    private ClientController clientController;
    /**
     * Provides user with options and provides methods to add, 
     * update, delete and read client and project records, also to add 
     * employee to the project and display employees in the project. 
     * @param args[] commandline arguments.   
     */
    public static void main(String args[]) {    
        ClientView clientView = new ClientView();
        ProjectView projectView = new ProjectView();
        String choice ;
        while (Constant.TRUE) {
            System.out.println("\nEnter your choice :\n1.Add new client"  
                                  + "\n2.View all clients\n3.Update an "  
                                  + "client\n4.Delete an client\n5.Add address " 
                                  + "to client\n6.Update address\n7.Client " 
                                  + "Operations\n8.Project operations\n9.Exit");
            choice = scanner.next();
            if (CommonUtil.isNumeric(choice)) {
                switch(Integer.parseInt(choice)) {
                    case 1:
                        clientView.addClient();
                        break;    
                    case 2:
                        clientView.displayClients();
                        break;
                    case 3:
                        clientView.updateEmail();
                        break;
                    case 4:
                        clientView.deleteClient();
                        break;
                    case 5: 
                        clientView.addAddress();
                        break;
                    case 6: 
                        clientView.updateAddress();
                        break;
                    case 7: 
                        clientView.clientOperations();
                        break;                    
                    case 8:
                        projectView.processProjects();
                        break;
                    case 9:
                        System.exit(Constant.ZERO);
                    default:
                        System.out.println("Wrong choice !"); 
                }
            } else {
                System.out.println(Constant.INVALID_NO);
            }
        } 
    }

    /**
     * Gets client id and checks client's status such as no of projects a 
     * client is owning with the given client id and returns the status.
     * @return status - either client existing or not with no of projects and 
     *                      success or error message
     */
    public Map<String, String> checkClientStatus() {
        ProjectController projectController = new ProjectController();
        clientController = new ClientController();
        System.out.println("\nEnter client id");
        String clientId = scanner.next();
        Map<String, String> status = clientController.hasClientForId(clientId);
        if(status.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) 
        {
            try {
                status.put(Constant.NO_OF_PROJECTS,String.valueOf( 
                               projectController.noOfProjectByClientId(
                                Integer.parseInt(clientId))));
                status.put(Constant.CLIENT_ID, clientId);
            } catch (AppException exception) {
                System.out.println(exception.getMessage());
            }   

        } 
        return status;        
    }

    /**
     * Gets client details from the user, adds the client to client record 
     * and displays success message if added successfully else failure message.
     */
    private void addClient() {  
        ClientView clientView = new ClientView();
        ClientController clientController = new ClientController();
        System.out.println("Do you want to add project ?\nEnter yes or no");
        String choice = scanner.next();
        if(choice.equalsIgnoreCase("yes")) {
            System.out.println("\nEnter client name");
            String name = scanner.next();
            System.out.println("\nEnter client email");
            String email = scanner.next();
            Address address = this.getAddress();
            Map<String, Object> status = new HashMap<>();
            try {
                status = clientController.addClient(new Client(name, email, 
                                                       address));    
            } catch(AppException e) {
                System.out.println(e.getMessage());
            } 
        } else if (choice.equalsIgnoreCase("no")) {
            System.out.println("Thank you.");
        } else {
            System.out.println("Invalid choice.");
        }
    }

    /**
     * Provides user with the options to perform client operations on projects.
     */
    private void clientOperations() {
        ProjectView projectView = new ProjectView();
        clientController = new ClientController();
        System.out.println("\nEnter a client id:");
        String id = scanner.next();
        Map<String, String> status = clientController.hasClientForId(id);

        if(status.get(Constant.ERROR_STATE).equals(Constant.FAILURE)) {
            System.out.println(status.get(Constant.MESSAGE));
        } else {
            String choice;
            while (Constant.TRUE) {
                System.out.println("\nEnter your choice:\n1.Get all projects of" 
                                      + " client\n2.Search by id\n3.Search by " 
                                      + "name \n4.Exit" );
                choice = scanner.next();
                if (CommonUtil.isNumeric(choice)) {
                    switch(Integer.parseInt(choice)) {
                        case 1:
                            projectView.displayAllProjectsByClient(
                                            Integer.parseInt(id));
                            break;    
                        case 2:
                            projectView.displayProjectById(Integer.parseInt(
                                                               id));
                            break;
                        case 3:
                            projectView.displayProjectByName(Integer.parseInt(
                                                                id));
                            break;
                        case 4:
                            System.exit(Constant.ZERO);
                        default:
                            System.out.println("Wrong choice !"); 
                    }
                } else {
                    System.out.println(Constant.INVALID_NO);
                }
            } 
        }               
    }

    /**
     * Gets client's address and adds address to clients record.
     */
    private void addAddress() { 
        ClientView clientView = new ClientView();
        clientController = new ClientController();
        System.out.println("\nEnter client id");
        String id = scanner.next();
        Map<String, String> clientStatus = clientController.hasClientForId(id);
        if(clientStatus.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) 
        { 
            AddressView addressView = new AddressView();
            addressView.addAddress(Integer.parseInt(id), Constant.CLIENT);
        } else {
            System.out.println(clientStatus.get(Constant.MESSAGE));
        }  
    }

    /**
     * Takes owner's id and owner type,gets address details from the user and 
     * returns it as address object.
     * @param ownerId  id of the owner which is unique to every owner 
     * @param owner owner who owns the address may be client, employee or 
     *                    employer
     * @param type type of the address either primary or secondary
     * @return Address  address, postal address of the owner which can be used to 
     *          communicate him / her. 
     */
    public Address getAddress() {
        System.out.println("Enter address line 1");
        String doorNo = scanner.next();
        System.out.println("Enter street");
        String street = scanner.next();
        System.out.println("Enter city");
        String city = scanner.next();
        System.out.println("Enter zipcode");
        String zipcode = scanner.next();
        return new Address(doorNo, street, city, zipcode);    
    }

    /**
     * Takes client's id and gets address to add it to the address record.
     */
    private void addAddress(int clientId) {
        AddressView addressView = new AddressView();
        addressView.addAddress(clientId, Constant.CLIENT);
    }

    /**
     * Gets employee's address and updates it in the address record.
     */
    private void updateAddress() {
        ClientView clientView = new ClientView();
        clientController = new ClientController();
        System.out.println("\nEnter client id");
        String id = scanner.next();
        Map<String, String> clientStatus = clientController.hasClientForId(id);
        if(clientStatus.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) 
        { 
            AddressView addressView = new AddressView();
            addressView.updateAddress(Integer.parseInt(id), Constant.CLIENT);
        } else {
            System.out.println(clientStatus.get(Constant.MESSAGE));
        }  
    }

    /**
     * Gets all client details from the client record and displays. 
     */
    private void displayClients() {
        clientController = new ClientController();
        try {
            List<Client> clients = clientController.getAllClients();
            for(Client client : clients) {
                System.out.println("id : " + client.getId() + "\nName : " 
                                       + client.getName() + "\nEmail : " 
                                       + client.getEmail());
                /*for(Address address : client.getAddresses()) {
                    System.out.println("\t******************");
                    System.out.println("\tNo : " + address.getDoorNo()
                                          + "\n\tStreet : " 
                                          + address.getStreet() 
                                          + "\n\tCity : " 
                                          + address.getCity() 
                                          + "\n\tZipcode : " 
                                          + address.getZipcode());                    
                }*/
            }
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }
    }

     /**
      * Gets client id and deletes a client from the client record and 
      * displays the success message if deleted, else error message.
      */
    private void deleteClient() {
        clientController = new ClientController();
        System.out.println("\nEnter client id");
        String id = scanner.next();
        if (CommonUtil.isNumeric(id)) { 
            try { 
                Client client = clientController.getClientById(Integer.parseInt(id));
                System.out.println(clientController.deleteClientById(client));
            } catch (AppException e) {
                System.out.println(e.getMessage());
            }        
        } else { 
            System.out.println("Invalid number.");
        }
    }

    /**
     * Gets client id, email then updates email and displays success message
     * if updated otherwise, displays error message. 
     */
    private void updateEmail() {
        clientController = new ClientController();
        System.out.println("\nEnter a client id:");
        String id = scanner.next();
        if(CommonUtil.isNumeric(id)) {
            System.out.println("\nEnter a email (valid character sequence):");
            String email = scanner.next();
            try {
                Client client = clientController.getClientById(Integer.parseInt(id));
                client.setEmail(email);
                System.out.println(clientController.updateEmail(client));
            } catch (AppException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Invalid no.");
        }
    }
}
