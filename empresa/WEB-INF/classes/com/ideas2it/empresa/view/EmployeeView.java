package com.ideas2it.empresa.view;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.controller.EmployeeController;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.view.ProjectView;

/**
 * The EmployeeView acts as an interface to the user and provides the user with 
 * options of the operations. It gets input from the user for the operations 
 * then responds to the user. 
 * 
 * @author Rajasekar
 * created on 12/07/2017
 */
public class EmployeeView {
    private static Scanner scanner = new Scanner(System.in);
    private EmployeeController employeeController;                               
    /**
     * Provides user with options and provides methods to add, 
     * update, delete and read employee and project records, also to add 
     * employee to the project and display employees in the project.  
     * @param args[] commandline arguments.   
     */
    public static void main(String args[]) {
        EmployeeView employeeView = new EmployeeView();
        ProjectView projectView = new ProjectView();
        String choice;
        while (Constant.TRUE) {
            System.out.println("\nEnter your choice :\n1.Add new employee"  
                                  + "\n2.View all employees\n3.Update an "  
                                  + "employee\n4.Delete an employee\n5.Add " 
                                  + "address to employee\n6.Update address"  
                                  + "\n7.Project operations\n8.Exit"); 
            choice = scanner.next();
            if (CommonUtil.isNumeric(choice)) {
                switch(Integer.parseInt(choice)) {
                    case 1:
                        employeeView.addEmployee();
                        break;    
                    case 2:
                        employeeView.displayEmployees();
                        break;
                    case 3:
                        employeeView.updateEmployee();
                        break;
                    case 4:
                        employeeView.deleteEmployeeById();
                        break;
                    case 5:
                        employeeView.addAddress();
                        break;
                    case 6:
                        employeeView.updateAddress();
                        break;
                    case 7:
                        projectView.processProjects();
                        break;
                    case 8:
                        System.exit(Constant.ZERO);
                    default:
                        System.out.println(Constant.INVALID_CHOICE); 
                }
            } else {
                System.out.println(Constant.INVALID_NO);
            }
        }
    }

    /**
     * Gets employee details from the user, adds the employee to employee record 
     * and displays error or success message.
     */
    private void addEmployee() {
        employeeController = new EmployeeController();
        EmployeeView employeeView = new EmployeeView();

        System.out.println("\nEnter employee name :");
        String name = scanner.next();
        System.out.println("\nEnter employee email :");
        String email = scanner.next();
        System.out.println("\nEnter employee phone :");
        String phone = scanner.next();	
        System.out.println("\nEnter DOB as YYYY-MM-DD :");
        String DOB = scanner.next();
        try {        
        Designation designation = employeeView.getDesignation();
        Address address = employeeView.getAddress();
            Map<String, Object> status = employeeController.addEmployee(
                                           new Employee(name, email, phone, DOB,       
                                           designation,address));
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Takes owner's id and owner type,gets address details from the user and 
     * returns it as address object.
     * @param ownerId  id of the owner which is unique to every owner 
     * @param owner owner who owns the address may be client, employee or 
     *                    employer
     * @param type type of the address either primary or secondary
     * @return Address  address, postal address of the owner which can be used to 
     *          communicate him / her. 
     */
    public Address getAddress() {
        System.out.println("Enter address line 1");
        String doorNo = scanner.next();
        System.out.println("Enter street");
        String street = scanner.next();
        System.out.println("Enter city");
        String city = scanner.next();
        System.out.println("Enter zipcode");
        String zipcode = scanner.next();
        return new Address(doorNo, street, city, zipcode);    
    }

    /**
     * Gets employee's id, address and adds address to employees record.
     */
    private void addAddress() { 
        EmployeeView employeeView = new EmployeeView();
        employeeController = new EmployeeController();
        System.out.println("\nEnter employee id:");
        String id = scanner.next();
        Map<String, String> status = employeeController.getEmployeeById(id);

        if((Constant.SUCCESS).equals(status.get(Constant.ERROR_STATE))) { 
            AddressView addressView = new AddressView();
            addressView.addAddress(Integer.parseInt(id), Constant.EMPLOYEE);
        } else {
            System.out.println(status.get(Constant.MESSAGE));
        }   
    }

    /**
     * Takes employee's id and gets address and adds address to employees record
     */
    private void addAddress(int employeeId) {
        AddressView addressView = new AddressView();    
        addressView.addAddress(employeeId, Constant.EMPLOYEE);
    }

    /**
     * Gets employee's id, address and then updates the employee's address
     */
    private void updateAddress() {
        EmployeeView employeeView = new EmployeeView();
        employeeController = new EmployeeController();
        System.out.println("\nEnter employee id:");
        String id = scanner.next();
        Map<String, String> status = employeeController.getEmployeeById(id);

        if((Constant.SUCCESS).equals(status.get(Constant.ERROR_STATE))) { 
            AddressView addressView = new AddressView();
            addressView.updateAddress(Integer.parseInt(id), Constant.EMPLOYEE);
        } else {
            System.out.println(status.get(Constant.MESSAGE));
        }  
    }

    /**
     * Gets employee id and checks whether employee exists for id. If not 
     * displays error message if exists provides options to update name, email & 
     * phone no. 
     */
    private void updateEmployee() {
        employeeController = new EmployeeController();
        EmployeeView employeeView = new EmployeeView();
        //employeeView.displayEmployees();
        System.out.println("\nEnter employee id:");
        String id = scanner.next();
        try { 
            Employee employee = employeeController.getEmployeeById(
                                    Integer.parseInt(id));
            String choice;
            do {
                System.out.println("\nEnter your choice :\n1.Update name"  
                                      + "\n2.Update email" 
		        	                  + "\n3.Update phone"
                                      + "\n4.Exit");
                choice = scanner.next();
                if (CommonUtil.isNumeric(choice)) {
                    switch(Integer.parseInt(choice)) {
                        case 1:
                            employeeView.updateNameById(employee);
                            break;    
                        case 2:
                            employeeView.updateEmailById(employee);
                            break;
                        case 3:
                            employeeView.updatePhoneById(employee);
                            break;             
                        default:
                            System.out.println(Constant.INVALID_CHOICE); 
                    }
                } else {
                    System.out.println(Constant.INVALID_NO);
                }            
            } while(Integer.parseInt(choice) != 4);            
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }
    }
                                                                                    
    /**
     * Takes employee id and updates name and displays success message if 
     * updated otherwise displays error message.
     * @param id    id of the employee to be updated. 
     */
    private void updateNameById(Employee employee) {
        employeeController = new EmployeeController();
        System.out.println("\nEnter a name (valid character sequence):");
        String name = scanner.next();
        try {
            employee.setName(name);
            System.out.println(employeeController.updateNameById(employee));											   
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }            
    }

    /**
     * Takes employee id and updates phone and displays success message if 
     * updated otherwise displays error message.
     * @param id    id of the employee to be updated. 
     */
    private void updatePhoneById(Employee employee) {
        employeeController = new EmployeeController();
        System.out.println("\nEnter a phone:");
        String phone = scanner.next();
        try {
            employee.setPhone(phone);
            System.out.println(employeeController.updatePhoneById(employee));											   
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Takes employee id and updates email and displays success message if 
     * updated otherwise displays error message.
     * @param id    id of the employee to be updated. 
     */
    private void updateEmailById(Employee employee) {
        employeeController = new EmployeeController();
        System.out.println("\nEnter a email :");
        String email = scanner.next();
        try {
            employee.setEmail(email);
            System.out.println(employeeController.updateEmailById(employee));											   
        } catch (AppException e) {
            System.out.println(e.getMessage());
        }
    }
                                                                                    
    /**
     * Gets employee id and checks whether employee exists for the id. 
     * If not exists displays failure message if exists deletes from the record 
     * and displays whether deleted or not.  
     */ 
    private void deleteEmployeeById() {
        employeeController = new EmployeeController();
        System.out.println("\nEnter employee id:");
        String id = scanner.next();
        if(CommonUtil.isNumeric(id)) {
            try { 
                Employee employee = employeeController.getEmployeeById(
                                        Integer.parseInt(id));
                System.out.println(employeeController.deleteEmployeeById(
                                       employee));  
            } catch (AppException e) {
                System.out.println(e.getMessage());
            }              
        } else {
            System.out.println(Constant.INVALID_NO);
        }
    }

    /**
     * Gets all employee records from the employee record and displays. 
     */
    private void displayEmployees() {
        employeeController = new EmployeeController();
        try {
            for(Employee employee : employeeController.getAllEmployees()) {
                System.out.println("\tNo : " + employee.getId()
                                  + "\n\tName : " + employee.getName() 
                                  + "\n\tDOB : " + employee.getDOB() 
                                  + "\n\tEmail : " + employee.getEmail()
                                  + "\n\tPhone : " + employee.getPhone()); 
                /*for(Address address : employee.getAddresses()) {
                    System.out.println("\t******************");
                    System.out.println("\tNo : " + address.getDoorNo()
                                          + "\n\tStreet : " 
                                          + address.getStreet() 
                                          + "\n\tCity : " 
                                          + address.getCity() 
                                          + "\n\tZipcode : " 
                                          + address.getZipcode());
                }*/
                for(Project project : employee.getProjects()) {
                    System.out.println("\t******************");
                    System.out.println(project);
                }
                System.out.println("----------------------------------------");
            }
        } catch (AppException appException) {
            System.out.println(appException.getMessage());
        }
    }

    /**

     */    
    private void displayDesignation() {
        
    }

    /**
     * Gets all designations available and displays and returns the designation 
     * for the given id. 
     * @return designation for the given id 
     * @throws AppException
     */
    private Designation getDesignation() throws AppException {
        employeeController = new EmployeeController();
        List<Designation> designations;
        Designation designation = null;
        designations = employeeController.getAllDesignations();
        for(Designation d : designations) {
            System.out.println(d.getId() + " : "  
                                   + d.getDesignation());
        }

        System.out.println("Enter designation id:");
        String designationId = scanner.next();
        
        if(CommonUtil.isNumeric(designationId)) {
            for (Designation d : designations) {
                if(Integer.parseInt(designationId) == d.getId()) {
                    designation = d;
                }
            }
        } else {
            throw new AppException(Constant.INVALID_NO);
        }
        if (null == designation) {
            throw new AppException("No designation found for the id");
        }
        return designation;
    }
} 
