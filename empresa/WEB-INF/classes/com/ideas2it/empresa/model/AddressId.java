package com.ideas2it.empresa.model;

import java.io.Serializable;

/*
 * The Address is model of address entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class AddressId implements Serializable { 
    private int ownerId;
    private String owner; 
    private String type;

    public AddressId () { }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }     

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getOwnerId() {
        return this.ownerId;
    } 

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return this.owner;
    } 
}
