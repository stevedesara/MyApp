package com.ideas2it.empresa.model;

import java.io.Serializable;

import com.ideas2it.empresa.model.AddressId;
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.model.Employee;

/*
 * The Address is model of address entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class Address implements Serializable    { 
    private int id;
    private int ownerId;
    private String doorNo;
    private String street;
    private String city;
    private String zipcode;
    private String type;
    private String owner;
    private AddressId aid; 
    private Employee employee;
    private Client client;

    public Address () {}

    /**
     * @param doorNo of the postal address
     * @param street of the postal address
     * @param city of the postal address
     * @param zipcode of the postal address
     * @param ownerId - owner's id which is unique to every owner.
     * @param owner - may be client, employee, employer whoever owns the address.
     */
    public Address (String doorNo, String street, String city, String zipcode, 
        int ownerId, String owner) 
    {
        this.doorNo = doorNo; 
        this.street = street; 
        this.city = city;
        this.zipcode = zipcode;
        this.ownerId = ownerId;
        this.owner = owner;
    }

    /**
     * @param doorNo of the postal address
     * @param street of the postal address
     * @param city of the postal address
     * @param zipcode of the postal address
     * @param ownerId - owner's id which is unique to every owner
     * @param owner - may be client, employee, employer whoever owns the address
     * @param type - type of the address either primary or secondary
     */
    public Address (String doorNo, String street, String city, String zipcode, 
        int ownerId, String owner, String type) 
    {
        this.doorNo = doorNo; 
        this.street = street; 
        this.city = city;
        this.zipcode = zipcode;
        this.ownerId = ownerId;
        this.owner = owner;
        this.type = type;
    }

    /**
     * @param doorNo of the postal address
     * @param street of the postal address
     * @param city of the postal address
     * @param zipcode of the postal address
     */
    public Address (String doorNo, String street, String city, String zipcode) {
        this.doorNo = doorNo; 
        this.street = street; 
        this.city = city;
        this.zipcode = zipcode;
    }

    /**
     * @param ownerId - id of the owner
     * @param owner - may be client, employee, employer whoever owns the address
     * @param type - type of the address either primary or secondary
     */
    public Address (int ownerId, String owner, String type) {
        this.ownerId = ownerId;
        this.owner = owner;
        this.type = type;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    } 

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    } 

    public void setAid(AddressId aid) {
        this.aid = aid;
    }

    public AddressId getAid () {
        return this.aid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }    

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public String getDoorNo() {
        return this.doorNo;
    } 

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return this.street;
    } 

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    } 

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return this.zipcode;
    } 

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getOwnerId() {
        return this.ownerId;
    } 

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return this.owner;
    } 

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public String toString () {
        return "\tNo : " + getDoorNo()
                                  + "\n\tStreet : " + getStreet() 
                                  + "\n\tCity : " + getCity() 
                                  + "\n\tZipcode : " + getZipcode() ;
    }
} 

